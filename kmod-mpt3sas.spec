%global pkg mpt3sas
%global kernel kernel version
%define pkg_version 43.00.00.00
%define anolis_release 1

%global debug_package %{nil}

Name:        kmod-%{pkg}
Version:     %(echo %{kernel} | sed -E 's/-/~/g; s/\.(an|al)[0-9]+$//g')
Release:     %{pkg_version}~%{anolis_release}%{?dist}
Summary:     LSI MPT Fusion drivers for SAS 3.0
License:     GPL-2.0
URL:         https://www.broadcom.cn/products/storage/host-bus-adapters/sas-nvme-9500-8i
Source0:     kmod-%{pkg}-%{pkg_version}.tar.gz

# 安装依赖, 和内核版本对应
Requires:            kernel >= %{kernel}
Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod
Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules
Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort

# 构建依赖, 和内核版本对应
BuildRequires:    kernel-devel   = %{kernel}
BuildRequires:    kernel-headers = %{kernel}
BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    system-rpm-config

Provides: kmod-%{pkg}-%{kernel}.%{_arch} = %{version}-%{release}
Obsoletes: kmod-%{pkg}-%{kernel}.%{_arch} < %{version}-%{release}

%description
RPM Package for Drivers for the LSI Corporation Mpt3sas  Architecture

# prep字段无需修改，保持不变即可
%prep
%setup -q -n kmod-%{pkg}-%{pkg_version}

%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd

%install
mkdir -p %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/mpt3sas
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/mpt3sas src/mpt3sas.ko

# install的该段需要保留，weak-module相关
# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+

# Generate depmod.conf
%{__install} -d %{buildroot}/%{_sysconfdir}/depmod.d/
for kmod in $(find %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra -type f -name \*.ko -printf "%%P\n" | sort)
do
echo "override $(basename $kmod .ko) * weak-updates/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
echo "override $(basename $kmod .ko) * extra/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
done

%clean
%{__rm} -rf %{buildroot}

%post
depmod -a > /dev/null 2>&1

if [ -x "/usr/sbin/weak-modules" ]; then
    printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/mpt3sas/mpt3sas.ko" | /usr/sbin/weak-modules --no-initramfs --add-modules
fi

%preun
echo "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/mpt3sas/%{pkg}.ko" >> /var/run/rpm-%{pkg}-modules.list

%postun
depmod -a > /dev/null 2>&1

if [ -x "/usr/sbin/weak-modules" ]; then
    modules=( $(cat /var/run/rpm-%{pkg}-modules.list) )
    printf '%s\n' "${modules[@]}" | /usr/sbin/weak-modules --no-initramfs --remove-modules
fi
rm /var/run/rpm-%{pkg}-modules.list


%files
%defattr(644,root,root,755)
%license licenses
/lib/modules/%{kernel}.%{_arch}
%config(noreplace) %{_sysconfdir}/depmod.d/%{pkg}.conf

%changelog
* Tue Nov 7 2023 Guixin Liu <kanie@linux.alibaba.com> - 43.00.00.00
- change spec weak-module (Guixin Liu)
